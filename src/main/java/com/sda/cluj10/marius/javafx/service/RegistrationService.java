package com.sda.cluj10.marius.javafx.service;

import com.sda.cluj10.marius.javafx.dao.RegistrationDao;
import com.sda.cluj10.marius.javafx.model.Registration;

import java.util.List;

public class RegistrationService {


    private RegistrationDao registrationDao= new RegistrationDao();

    public List<Registration> getAllRegistrations(){
        List<Registration> allRegistrations=registrationDao.getAllRegistrations();

        return  allRegistrations;
    }


}
