package com.sda.cluj10.marius.javafx.model;


import com.sun.javafx.beans.IDProperty;

import javax.persistence.*;

@Entity
@Table(name = "membership_types")
public class MemberShipType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_membership_types")
    private int membershiptypesid;
    @Column(name = "type_name")
    private String typename;
    @Column(name = "price")
    private int price;


    public int getMembershiptypesid() {
        return membershiptypesid;
    }

    public void setMembershiptypesid(int membershiptypesid) {
        this.membershiptypesid = membershiptypesid;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
