package com.sda.cluj10.marius.javafx.dao;

import com.sda.cluj10.marius.javafx.model.Schedule;
import com.sda.cluj10.marius.javafx.model.Trainer;
import org.hibernate.Session;
import org.hibernate.SessionBuilder;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;

import java.util.List;

public class TrainerDao {

    private SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

    public List<Trainer> getAllTrainers() {
        //TODO:use session and Transaction
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        Query<Trainer> query = session.createQuery("from Trainer");
        List<Trainer> trainers = query.list();
        tx.commit();
        session.close();
        return null;

    }


    }





