package com.sda.cluj10.marius.javafx.dao;

import com.sda.cluj10.marius.javafx.model.MemberShip;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class MemberShipDao {

    private SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

    public List<MemberShip> getAllMemberships() {

        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        Query<MemberShip> query = session.createQuery("from MemberShip");
        List<MemberShip> memberShips = query.list();
        tx.commit();
        session.close();

        return null;


    }


    }

