package com.sda.cluj10.marius.javafx.dao;

import com.sda.cluj10.marius.javafx.model.Registration;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.engine.config.spi.ConfigurationService;
import org.hibernate.query.Query;

import java.util.List;

public class RegistrationDao {

    private SessionFactory sessionFactory=new Configuration().configure().buildSessionFactory();

    public List<Registration> getAllRegistrations(){
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        Query<Registration> query=session.createQuery("from Registration");
        List<Registration> registrations=query.list();
        tx.commit();
        session.close();
        return null;
    }

}
