package com.sda.cluj10.marius.javafx.dao;

import com.sda.cluj10.marius.javafx.model.MemberShipType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class MemberShipTypeDao {

    private SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();


    public List<MemberShipType> getAllMembershipTypes(){
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        Query<MemberShipType> query=session.createQuery("from MemberShipType");
        List<MemberShipType> memberShipTypes=query.list();
        tx.commit();
        session.close();
        return null;
    }

}
