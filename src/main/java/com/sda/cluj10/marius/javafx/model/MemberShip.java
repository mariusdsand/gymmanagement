package com.sda.cluj10.marius.javafx.model;


import javax.persistence.*;

@Entity
@Table(name = "membership")
public class MemberShip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int membershipid;
    @Column(name = "client_id")
    private int clientid;
    @Column(name = "membership_type_id")
    private int membershiptypeid;
    @Column(name = "start_date")
    private int startdate;
    @Column(name = "end_date")
    private int enddate;
    @Column(name = "transaction_date")
    private int transactiondate;




    public int getClient_id() {
        return clientid;
    }

    public void setClient_id(int clientid) {
        this.clientid = clientid;
    }

    public int getMembership_type_id() {
        return membershiptypeid;
    }

    public void setMembership_type_id(int membership_type_id) {
        this.membershiptypeid = membership_type_id;
    }

    public int getStart_date() {
        return startdate;
    }

    public void setStart_date(int start_date) {
        this.startdate = start_date;
    }

    public int getEnd_date() {
        return enddate;
    }

    public void setEnd_date(int end_date) {
        this.enddate = end_date;
    }


    public int getTransaction_date() {
        return transactiondate;
    }

    public void setTransaction_date(int transactiondate) {
        this.transactiondate = transactiondate;
    }
}
