package com.sda.cluj10.marius.javafx.service;

import com.sda.cluj10.marius.javafx.dao.MemberShipDao;
import com.sda.cluj10.marius.javafx.dao.MemberShipTypeDao;
import com.sda.cluj10.marius.javafx.model.MemberShipType;

import java.util.List;

public class MemberShipTypeService {

    private MemberShipTypeDao memberShipTypeDao = new MemberShipTypeDao();

    public List<MemberShipType> getAllMembershipTypes() {
        List<MemberShipType> allmembershiptypes = memberShipTypeDao.getAllMembershipTypes();

        return allmembershiptypes;
    }

}
