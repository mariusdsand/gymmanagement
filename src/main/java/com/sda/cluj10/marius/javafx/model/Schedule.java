package com.sda.cluj10.marius.javafx.model;


import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Table(name = "schedule")
public class Schedule {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name="id")
    private int scheduleid;
@Column(name="class_id")
    private int classid;
@Column(name="trainer_id")
    private int trainerid;
@Column(name="date_time")
    private LocalDateTime date_time;
@Column(name="availability")
    private int availability;
@Column(name="subscription_start_time")
    private LocalTime subcribestarttime;
@ManyToOne
@JoinColumn(name = "trainer_id")


    public int getScheduleid() {
        return scheduleid;
    }

    public void setScheduleid(int scheduleid) {
        this.scheduleid = scheduleid;
    }

    public int getClassid() {
        return classid;
    }

    public void setClassid(int classid) {
        this.classid = classid;
    }

    public int getTrainerid() {
        return trainerid;
    }

    public void setTrainerid(int trainerid) {
        this.trainerid = trainerid;
    }

    public LocalDateTime getDate_time() {
        return date_time;
    }

    public void setDate_time(LocalDateTime date_time) {
        this.date_time = date_time;
    }

    public int getAvailibility() {
        return availability;
    }

    public void setAvailibility(int availibility) {
        this.availability = availibility;
    }

    public LocalTime getSubcribe_start_time() {
        return subcribestarttime;
    }

    public void setSubcribe_start_time(LocalTime subcribe_start_time) {
        this.subcribestarttime = subcribe_start_time;
    }

    


}
