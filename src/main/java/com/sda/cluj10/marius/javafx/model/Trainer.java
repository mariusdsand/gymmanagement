package com.sda.cluj10.marius.javafx.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "trainers")
public class Trainer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "trainer_id")
    private long trainerid;
    @Column(name = "first_name")
    private String firstname;
    @Column(name = "last_name")
    private String lastname;
    @Column(name = "experience")
    private int experience;
    @OneToMany(mappedBy = "trainers")
    public String getFirst_name() {
        return firstname;
    }

    public String getLast_name() {
        return lastname;
    }

    public int getExperience() {
        return experience;
    }

    public void setFirst_name(String first_name) {
        this.firstname = first_name;
    }

    public void setLast_name(String last_name) {
        this.lastname = last_name;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public long getTrainerid() {
        return trainerid;
    }

    public void setTrainerid(int trainerid) {
        this.trainerid = trainerid;
    }


    public void setTrainerid(long trainerid) {
        this.trainerid = trainerid;
    }


    @Override
    public String toString() {
        return "Trainer{" +
                "trainerid=" + trainerid +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", experience=" + experience +
                '}';
    }


}

