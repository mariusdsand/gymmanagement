package com.sda.cluj10.marius.javafx.service;

import com.sda.cluj10.marius.javafx.dao.ScheduleDao;
import com.sda.cluj10.marius.javafx.model.Schedule;

import java.util.List;

public class ScheduleService {

    private ScheduleDao scheduleDao=new ScheduleDao();

    public List<Schedule> getAllSchedules(){
        List<Schedule> allSchedules=scheduleDao.getAllSchedules();


        return  allSchedules;
    }

}
