package com.sda.cluj10.marius.javafx;

import com.sda.cluj10.marius.javafx.model.MemberShip;
import com.sda.cluj10.marius.javafx.model.Trainer;
import com.sda.cluj10.marius.javafx.service.*;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

// ca sa facem o aplicatie fx trebuie sa extindem in primul rand aplication fx.
public class MainApp extends Application {

    public static void main(String[] args) {

        TrainerService trainerService= new TrainerService();
        System.out.println(trainerService.getAllTrainers());

        MembershipService membershipService=new MembershipService();
        System.out.println(membershipService.getAllMemberships());

        MemberShipTypeService memberShipTypeService=new MemberShipTypeService();
        System.out.println(membershipService.getAllMemberships());

        RegistrationService registrationService=new RegistrationService();
        System.out.println(registrationService.getAllRegistrations());

        ScheduleService scheduleService=new ScheduleService();
        System.out.println(scheduleService.getAllSchedules());

        System.out.println("Started");
        launch();//aceasta metoda lanseaza/porneste aplicatia
        System.out.println("Ended");
    }

// lifecycle sunt 2 metode

    @Override
    public void init() throws Exception {
        System.out.println("Initing the app!");
        System.out.println("Loading files");
        System.out.println("Loading data from DB");
        ;
    }

    @Override
    public void stop() throws Exception {
        System.out.println("Closing App");
        System.out.println("Closing Files");
    }

    // prima metoda cea de start ce construieste elementele
    public void start(Stage primaryStage) {
        System.out.println("Starting JavaFX Application");
        primaryStage.setHeight(400);
        primaryStage.setWidth(700);// aici setam cat sa fie chenarul de lata si inalt
        primaryStage.setY(0);//astea sa setam unde sa porneasca aplicatia pe ecran in mijloc-sus-jos etc
        primaryStage.setX(500);
        //sa-l facem fullscreen-primaryStage.setFullScreen(true);

        primaryStage.setTitle("GYM Software");


        VBox rootVbox = new VBox();
        // butoane verticale -containere
        Button hellobutton = new Button("Hello!");
        Button loginbutton = new Button("Login");

        Button registerbutton = new Button("Register");
        // Label hellolabel = new Label("HelloWorld!");// sa fim atenti label javafx sa alegem tot timpu la orice nu awp
        //rootVbox.getChildren().add(hellobutton);//la add alegem NODE care este parintele parintilor
        // rootVbox.getChildren().add(loginbutton);
        // rootVbox.getChildren().add(registerbutton);
        //rootVbox.getChildren().addAll(hellobutton, loginbutton, registerbutton);//le adauga in linie in cod este mai bine

        rootVbox.setAlignment(Pos.TOP_CENTER);//muta butoanele in mijloc de exemplu adica aliniere
        rootVbox.setPadding(new Insets(50, 0, 0, 0));//este ca o rama, primeste 4 parametrii ce grosime sa aiba rama
        rootVbox.setSpacing(8);//spatiere intre butoane

//container butoane orizontale dar nu merge sa setam acelasi butoane in acelasi container ca nu va merge
        HBox hBox = new HBox();

        //     Button helloButton = new Button("HelloH!");
        //   Button loginButton = new Button("LoginH");
        // Button registerButton = new Button("RegisterH");
        // Text welcomeText = new Text("Welcome back ! ");
        //welcomeText.setFont(Font.font("veranda", 20));//font
        //welcomeText.setFill(Color.GREEN);//culoarea
        //welcomeText.setFill(Color.rgb(65, 78, 224));
        //hBox.getChildren().addAll(welcomeText, helloButton, loginButton, registerButton);
//Pentru user!!!!!!!!!!
        Label usernameLabel = new Label();
        usernameLabel.setText("Username: ");
        TextField usernametextField = new TextField();
        //
        usernametextField.setOnKeyPressed(event -> System.out.println("Pressed: " + event.getText()));


        //sa memomere parola si user de exemplu ca la site uri
        CheckBox rememberMeCheckBox = new CheckBox("Keep me logged in");

        HBox usernamehBox = new HBox();
        usernamehBox.getChildren().addAll(usernameLabel, usernametextField);


        usernamehBox.setAlignment(Pos.CENTER);
        usernamehBox.setPadding(new Insets(50, 20, 0, 0));
        usernamehBox.setSpacing(5);

        //password
        // PasswordField-hides input characters
//container ul gridpane / folosim mai mult sa facem tabele

        HBox passwordHbox = new HBox();
        Label passwordLabel = new Label();
        passwordLabel.setText("Password: ");
        PasswordField passwordField = new PasswordField();
        passwordField.setOnKeyPressed((event -> System.out.println("PassowrdKey:" + event.getText())));

        passwordHbox.getChildren().addAll(passwordLabel, passwordField);
        usernamehBox.setPadding(new Insets(50, 20, 0, 0));
        passwordHbox.setAlignment(Pos.CENTER);


        GridPane gridPane = new GridPane();
        gridPane.add(new Label("Marius"), 0, 0);// label se foloseste cand pune un text de input
        gridPane.add(new Label("Cristi"), 0, 1);
        gridPane.add(new Label("Paul"), 1, 0);
        gridPane.add(new Label("James"), 1, 1);

        gridPane.setPadding(new Insets(20, 20, 20, 20));
        gridPane.setAlignment(Pos.CENTER);

        //2 metode separate una pentru linii una pentru coloane
        gridPane.setHgap(50);//spatiere dar este diferite ca celelalte containere
        gridPane.setVgap(18);// spatiere pe verticala


        ListView<String> studentsListView = new ListView<String>();
        studentsListView.getItems().addAll("Marius", "Cristi", "Paul", "Ana");
        studentsListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        ComboBox<String> studentsComboBox = new ComboBox<String>();
        studentsComboBox.getItems().addAll("Marius", "Cristi", "Paul", "Ana");

        Text loginText = new Text("Login to GYM App");
        loginText.setFont(Font.font(22));
        Text incorrectUsernameText = new Text("Incorrect Username");
        incorrectUsernameText.setVisible(false);
        loginbutton.setOnMouseClicked(event -> {
            System.out.println("Clicked Login");
            System.out.println("Username: " + usernametextField.getText());
            System.out.println("Password: " + passwordField.getText());
            //Hibernate GetUserByUserNameAndPassowrd(usernameTextField.getText(),passwordField.getText();
            if (usernametextField.getText().equalsIgnoreCase("mariust")) {


                Scene loggedInScene = new Scene(gridPane);
                primaryStage.setScene(loggedInScene);

            }else{
                incorrectUsernameText.setVisible(true);

            }

        });

        rootVbox.getChildren().addAll(loginText, usernamehBox, passwordHbox, rememberMeCheckBox, loginbutton);


        Scene scene = new Scene(rootVbox);


        primaryStage.setScene(scene);
        primaryStage.show();//aici arata tot ce am construit
    }
}
