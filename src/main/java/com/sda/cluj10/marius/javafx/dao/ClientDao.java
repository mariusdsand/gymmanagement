package com.sda.cluj10.marius.javafx.dao;

import com.sda.cluj10.marius.javafx.model.Client;
import com.sda.cluj10.marius.javafx.model.Trainer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class ClientDao {

    private SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();


    public List<Client> getAllClients(){
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        Query<Client> query=session.createQuery("from Client");
        List<Client> clients=query.list();
        tx.commit();
        session.close();
        return null;
    }

    }

