package com.sda.cluj10.marius.javafx.service;

import com.sda.cluj10.marius.javafx.dao.MemberShipDao;
import com.sda.cluj10.marius.javafx.model.MemberShip;

import java.util.List;

public class MembershipService {


    private MemberShipDao memberShipDao = new MemberShipDao();

    public List<MemberShip> getAllMemberships() {
        List<MemberShip> allMemberships = memberShipDao.getAllMemberships();

        return allMemberships;
    }
}
