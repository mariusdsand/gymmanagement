package com.sda.cluj10.marius.javafx.dao;

import com.sda.cluj10.marius.javafx.model.Schedule;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class ScheduleDao {
    private SessionFactory sessionFactory=new Configuration().configure().buildSessionFactory();

    public List<Schedule> getAllSchedules(){
        Session session=sessionFactory.openSession();
        Transaction tx=session.beginTransaction();

        Query<Schedule> query=session.createQuery("from Schedule");
        List<Schedule> schedules=query.list();
        tx.commit();
        session.close();
        return null;
    }

}
